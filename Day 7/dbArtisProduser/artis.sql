CREATE TABLE artis (
kd_artis VARCHAR(100) PRIMARY KEY NOT null,
nm_artis VARCHAR(100) not null,
jk VARCHAR(100) NOT null,
bayaran INTEGER NOT null,
award INTEGER NOT null,
negara VARCHAR(100) NOT null
)

INSERT INTO artis 
(kd_artis, nm_artis, jk, bayaran, award, negara)
VALUES
('A001','ROBERT DOWNEY JR','PRIA',500000000,2,'AS'),
('A002','ANGELINA JOLIE','PRIA',700000000,1,'AS'),
('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
('A005','CHELSEA ISLAN','PRIA',300000000,0,'ID')

SELECT * FROM artis