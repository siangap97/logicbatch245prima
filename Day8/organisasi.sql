CREATE TABLE organisasi (
	id INT PRIMARY KEY NOT NULL,
	kodeorganisasi VARCHAR(12) NOT NULL,
	namaorganisasi VARCHAR(100) NOT NULL,
	statusorganisasi VARCHAR(13) NOT NULL
)

INSERT INTO organisasi(kodeorganisasi, namaorganisasi, statusorganisasi)
VALUES
('KK001','Unit Kegiatan Mahasiswa (UKM)','Aktif'),
('KK002','Badan Eksekutif Mahasiswa Fakultas (BEMF)','Aktif'),
('KK003','Dewan Perwakilan Mahasiswa Universitas (DPMU)','Aktif'),
('KK004','Badan Eksekutif Mahasiswa Universitas (BEMU)','Non Aktif'),
('KK005','Himpunan Mahasiswa Jurusan','Non Aktif'),
('KK006','Himpunan Kompetisi Jurusan','Aktif')

--2. Ubah panjang karakter nama dosen menjadi 200
ALTER TABLE dosen ALTER COLUMN namadosen TYPE VARCHAR(200)
--3. tampilkan kode mahasiswa, nama mahasiswa, nama MataKuliah, deskripsi Jurusan Kuliah dengan kode mahasiswa  mk001
SELECT a.kodemahasiswa, a.namamahasiswa, b.namamatakuliah, c.deskripsi FROM mahasiswa AS a RIGHT JOIN matakuliah AS b ON a.kodematakuliah = b.kodematakuliah
JOIN jurusankuliah AS c ON a.kodejurusan = c.kodejurusan
--4. tampilkan nama Mata Kuliah, status Mata Kuliah, kode Mahasiswa, nama Mahasiswa, alamat Mahasiswa, kode Jurusan, kode Mata Kuliah dengan status Mata Kuliah tidak aktif
SELECT a.namamatakuliah, a.keaktifanstatus, b.kodemahasiswa, b.namamahasiswa, b.alamatmahasiswa, b.kodejurusan, b.kodematakuliah FROM matakuliah AS a
INNER JOIN mahasiswa AS b on a.kodematakuliah = b.kodematakuliah WHERE a.keaktifanstatus = 'Non Aktif'
--5. tampilakn kode mahasiswa, nama mahasiswa, alamat mahasiswa dengan status organisasi aktif dan nilai diatas 79
SELECT a.kodemahasiswa, a.namamahasiswa, a.alamatmahasiswa, b.nilai, c.statusorganisasi FROM mahasiswa AS a RIGHT JOIN nilaimahasiswa AS b ON a.kodemahasiswa = b.kodemahasiswa
INNER JOIN organisasi AS c ON b.kodeorganisasi = c.kodeorganisasi WHERE nilai > 79
--6. tampilkan kode Mata Kuliah, nama Mata Kuliah, status Mata Kuliah dengan nama Matakuliah mengandung huruf ‘n’
SELECT * FROM matakuliah WHERE namamatakuliah LIKE '%N%'
--7. tampilkan  nama mahasiswa yang memiliki organisasi lebih dari 1
SELECT COUNT(b.kodeorganisasi) AS jumlahorganisasi, a.namamahasiswa FROM mahasiswa AS a INNER JOIN nilaimahasiswa AS b ON a.kodemahasiswa = b.kodemahasiswa 
JOIN organisasi AS c ON b.kodeorganisasi = c.kodeorganisasi GROUP BY a.namamahasiswa HAVING COUNT(b.kodeorganisasi) > 1
--8. tampilkan kode mahasiswa, nama mahasiswa, nama mata kuliah, deskripsi jurusan, nama dosen, status mata kuliah, deskripsi fakultas dengan kode mahasiswa mk001
SELECT a.kodemahasiswa, b.namamahasiswa, b.kodematakuliah, c.namamatakuliah, c.keaktifanstatus, d.deskripsi FROM nilaimahasiswa AS a INNER JOIN mahasiswa as b ON a.kodemahasiswa = b.kodemahasiswa
INNER JOIN matakuliah AS c ON b.kodematakuliah = c.kodematakuliah JOIN jurusankuliah AS d ON b.kodejurusan = d.kodejurusan WHERE a.kodemahasiswa = 'MK001'
--9. buat view dengan kode mahasiswa, nama mahasiswa, nama mata kuliah, nama dosen, status matakuliah, deskripsi fakultas 
SELECT a.kodemahasiswa, a.namamahasiswa, b.namamatakuliah, b.keaktifanstatus, c.namadosen, e.penjelasan FROM mahasiswa AS a INNER JOIN matakuliah AS b ON a.kodematakuliah = b.kodematakuliah
INNER JOIN dosen AS c ON b.kodematakuliah = c.kodematakuliah INNER JOIN fakultas AS e ON c.kodefakultas = e.kodefakultas
--10. tampilkan seluruh data dari virtual table dimana kode mahasiswa MK001
SELECT * FROM mahasiswa AS A LEFT JOIN nilaimahasiswa AS b  ON a.kodemahasiswa = b.kodemahasiswa JOIN jurusankuliah AS c ON a.kodejurusan = c.kodejurusan JOIN matakuliah as d ON a.kodematakuliah = d.kodematakuliah
JOIN organisasi as e ON b.kodeorganisasi= e.kodeorganisasi JOIN dosen AS f ON d.kodematakuliah = f.kodematakuliah JOIN fakultas AS g
ON f.kodefakultas = g.kodefakultas WHERE a.kodemahasiswa = 'MK001'