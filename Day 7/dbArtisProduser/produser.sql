CREATE TABLE produser (
kd_produser VARCHAR(50) PRIMARY KEY NOT null,
nm_produser VARCHAR (50) NOT NULL,
international VARCHAR(50) NOT NULL
)

INSERT INTO produser (kd_produser, nm_produser, international)
VALUES
('PD01','MARVEL','YA'),
('PD02','HONGKONG CINEMA','YA'),
('PD03','RAPI FILM','TIDAK'),
('PD04','PARKIT','TIDAK'),
('PD05','PARAMOUNT PICTURE','YA')