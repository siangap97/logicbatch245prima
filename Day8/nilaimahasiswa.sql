CREATE TABLE nilaimahasiswa (
	id INT PRIMARY KEY NOT NULL,
	kodenilai VARCHAR(12) NOT NULL,
	kodemahasiswa VARCHAR(12) NOT NULL,
	kodeorganisasi VARCHAR(9) NOT NULL,
	nilai INT NOT NULL
)

INSERT INTO nilaimahasiswa(kodenilai, kodemahasiswa, kodeorganisasi, nilai)
VALUES
('SK001','MK004','KK001',90),
('SK002','MK001','KK001',80),
('SK003','MK002','KK003',85),
('SK004','MK004','KK002',95),
('SK005','MK005','KK005',70)