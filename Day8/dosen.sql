CREATE TABLE dosen (
	id INT PRIMARY KEY NOT NULL,
	kodedosen VARCHAR(14) NOT NULL,
	namadosen VARCHAR(15) NOT NULL,
	kodematakuliah VARCHAR(12) NOT NULL,
	kodefakultas VARCHAR(13) NOT NULL
)

INSERT INTO dosen (kodedosen, namadosen, kodematakuliah, kodefakultas)
VALUES
('GK001','Ahmad Prasetyo','KPK001','TK002'),
('GK002','Hadi Fuladi','KPK002','TK001'),
('GK003','Johan Goerge','KPK003','TK002'),
('GK004','Bima Darmawan','KPK004','TK002'),
('GK005','Gatot Wahyudi','KPK005','TK001')
