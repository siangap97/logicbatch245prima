CREATE TABLE genre (
kd_genre VARCHAR(50) PRIMARY KEY NOT null,
nm_genre VARCHAR(50) NOT NULL
)

INSERT INTO genre(kd_genre, nm_genre)
VALUES
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILER'),
('G006','FICTION')