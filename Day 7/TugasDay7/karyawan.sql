CREATE TABLE Karyawan (
	id BIGINT PRIMARY KEY NOT NULL,
	nomor_induk VARCHAR(7) NOT NULL,
	nama VARCHAR(30) NOT NULL,
	alamat TEXT NOT NULL,
	tanggal_lahir DATE NOT NULL, 
	tanggal_masuk DATE NOT NULL
)

CREATE VIEW vwKaryawan AS SELECT * FROM Karyawan

INSERT INTO Karyawan(nomor_induk, nama, alamat, tanggal_lahir, tanggal_masuk)
VALUES
('IP06001', 'Agus', 'Jln. Gajah Mada 115A, Jakarta Pusat', '1/8/70', '7/7/06'),
('IP06002', 'Amin', 'Jln. Bungur Sari V No. 178, Bandung, Jakarta Pusat', '3/5/77', '6/7/06'),
('IP06003', 'Yusuf', 'Jln. Yosodpuro 15, Surabaya', '9/8/73', '8/7/06'),
('IP07004', 'Alyssa', 'Jln. Cendana No. 6 Bandung', '14/2/83', '5/1/07'),
('IP07005', 'Maulana', 'Jln. Ampera Raya No 1', '10/10/85', '5/2/07'),
('IP07006', 'Afika', 'Jln. Pajetan Barat No. 6A', '9/3/87', '9/6/07'),
('IP07007', 'James', 'Jln. Padjajaran No. 111, Bandung', '19/5/88', '9/6/06'),
('IP09008', 'Oktavanus', 'Jln. Gajah Mada 101, Semarang', '7/10/88', '8/8/08'),
('IP09009', 'Nugroho', 'Jln. Duren Tiga 196, Jakarta Selatan', '20/1/88', '11/11/08'),
('IP09010', 'Raisa', 'Jln. Nangka, Jakarta Selatan', '29/12/89', '9/2/09')

SELECT * FROM Karyawan

