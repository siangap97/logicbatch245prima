CREATE TABLE matakuliah (
	id INT PRIMARY KEY NOT NULL,
	kodematakuliah VARCHAR(12) NOT NULL,
	namamatakuliah VARCHAR(100) NOT NULL,
	keaktifanstatus VARCHAR(15) NOT NULL
)

INSERT INTO matakuliah(kodematakuliah, namamatakuliah, keaktifanstatus)
values
('KPK001','Algoritma Dasar','Aktif'),
('KPK002','Basis Data','Aktif'),
('KPK003','Kalkulus','Aktif'),
('KPK004','Pengantar Bisnis','Aktif'),
('KPK005','Matematika Ekonomi & Bisnis','Non Aktif')

SELECT * FROM matakuliah
