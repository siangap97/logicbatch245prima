CREATE TABLE film (
kd_film VARCHAR(10) PRIMARY KEY NOT null,
nm_film VARCHAR(55) NOT null,
genre VARCHAR(55) NOT null,
artis VARCHAR(55) NOT null,
produser VARCHAR(55) NOT null,
pendapatan INTEGER NOT null,
nominasi INTEGER NOT null
)

INSERT INTO film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi)
values
('F001','IRON MAN','G001','A001','PD01',2000000000,3),
('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
('F004','AVENGER : CIVIL WAR','G001','A001','PD01',2000000000,1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
('F006','THE RAID','G001','A004','PD03',800000000,5),
('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
('F009','POLICE STORY','G001','A003','PD02',700000000,3),
('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5)

SELECT * FROM film

--1.  Menampilkan nama film dan nominasi dari nominasi yang paling besar
SELECT nm_film, nominasi FROM film ORDER BY nominasi DESC
--2.  Menampilkan nama film dan nominasi yang paling banyak memperoleh nominasi 
SELECT nm_film, nominasi FROM film WHERE nominasi = (SELECT MAX(nominasi) FROM film)
--3.  Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi 
SELECT nm_film, nominasi FROM film WHERE nominasi = 0
--4.  Menampilkan nama film dan pendapatan dari yang paling kecil
SELECT nm_film, pendapatan FROM film ORDER BY pendapatan ASC
--5.  Menampilkan nama film dan pendapatan yang terbesar
SELECT nm_film, pendapatan FROM film ORDER BY pendapatan DESC
--6.  Menampilkan nama film yang huruf depannya 'p' 
SELECT nm_film FROM film WHERE nm_film LIKE 'P%'
--7.  Menampilkan nama film yang huruf terakhir 'h' 
SELECT nm_film FROM film WHERE nm_film LIKE '%H'
--8.  Menampilkan nama film yang mengandung huruf 'd' 
SELECT nm_film FROM film WHERE nm_film LIKE '%D%'
--9.  Menampilkan nama film dengan pendapatan terbesar mengandung huruf 'o'
SELECT nm_film, pendapatan FROM film WHERE nm_film LIKE '%O%' AND pendapatan = (SELECT MAX(pendapatan) FROM film)
--10. Menampilkan nama film dengan pendapatan terkecil mengandung huruf 'o' 
SELECT nm_film, pendapatan FROM film WHERE nm_film LIKE '%O%' AND pendapatan = (SELECT MIN(pendapatan) FROM film)
--11. Menampilkan nama film dan artis
SELECT a.nm_artis, b.nm_film FROM artis AS a RIGHT JOIN film as b ON a.kd_artis = b.artis
--12. Menampilkan nama film yang artisnya berasal dari negara hongkong
SELECT a.nm_film, b.negara FROM film AS a INNER JOIN artis AS b ON b.kd_artis = a.artis WHERE b.negara like 'HONGKONG'
--13. Menampilkan nama film yang artisnya bukan berasal dari negara yang tidak mengandung huruf 'o'
SELECT a.nm_film, b.negara FROM film AS a INNER JOIN artis AS b ON b.kd_artis = a.artis WHERE b.negara not like '%O%'
--14. Menampilkan nama artis yang tidak pernah bermain film 
SELECT a.kd_artis, a.nm_artis, b.nm_film FROM artis AS a left JOIN film as b ON a.kd_artis = b.artis
group by a.kd_artis, a.nm_artis, b.nm_film Having count(b.nm_film) = 0
--15. Menampilkan nama artis yang bermain film dengan genre drama 
SELECT a.kd_artis, b.nm_film, c.nm_genre FROM artis AS a RIGHT JOIN film AS b
ON a.kd_artis = b.artis JOIN genre AS c ON b.genre = c.kd_genre WHERE nm_genre = 'DRAMA'
--16. Menampilkan nama artis yang bermain film dengan genre horror 
SELECT a.kd_artis, b.nm_film, c.nm_genre FROM artis AS a RIGHT JOIN film AS b ON a.kd_artis = b.artis JOIN genre AS c 
ON b.genre = c.kd_genre WHERE nm_genre = 'HORROR'
--17. Menampilkan data negara dengan jumlah filmnya
SELECT a.nm_negara, b.nm_artis, c.nm_film FROM negara AS a LEFT JOIN artis AS b ON a.kd_negara = b.negara
LEFT JOIN film as c ON b.kd_artis = c.artis
--18. Menampilkan nama produser yang skala internasional 
SELECT * FROM produser WHERE international = 'YA'
--19. Menampilkan jumlah film dari masing2 produser 
SELECT b.nm_produser, a.nm_film FROM film AS a LEFT JOIN produser AS b ON a.produser = b.kd_produser
SELECT COUNT(a.pendapatan) as jumlah, b.nm_produser FROM film AS a LEFT JOIN produser as b ON a.produser = b.kd_produser
GROUP BY b.nm_produser 
--20. Menampilkan jumlah pendapatan produser marvel secara keseluruhan 
SELECT SUM(a.pendapatan) as jumlah, b.nm_produser FROM film AS a LEFT JOIN produser as b ON a.produser = b.kd_produser
GROUP BY b.nm_produser having b.nm_produser = 'MARVEL'