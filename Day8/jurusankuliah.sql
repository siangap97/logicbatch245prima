CREATE TABLE jurusankuliah (
	id INT PRIMARY KEY NOT NULL,
	kodejurusan VARCHAR(12) NOT NULL,
	deskripsi VARCHAR(100) NOT NULL
)

INSERT INTO jurusankuliah(kodejurusan, deskripsi)
VALUES
('KA001','Teknik Informatika'),
('KA002','Management Bisnis'),
('KA003','Ilmu Komunikasi'),
('KA004','Sastra Inggris'),
('KA005','Ilmu Pengetahuan Alam Dan Matematika'),
('KA006','Kedokteran')

SELECT * FROM jurusankuliah