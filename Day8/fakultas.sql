CREATE TABLE fakultas (
	id INT PRIMARY KEY NOT NULL,
	kodefakultas VARCHAR(13) NOT NULL,
	penjelasan VARCHAR(30) NOT NULL
)

INSERT INTO fakultas(kodefakultas, penjelasan)
VALUES
('TK001','Teknik Informatika'),
('TK002','Matematika'),
('TK003','Sistem Informatika')