create table negara (
kd_negara VARCHAR(100) PRIMARY KEY NOT null,
nm_negara VARCHAR(100) NOT null
)

INSERT INTO negara(kd_negara, nm_negara)
VALUES
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')
