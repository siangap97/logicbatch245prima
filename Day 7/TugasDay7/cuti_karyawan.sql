CREATE TABLE cuti_karyawan(
	id BIGINT PRIMARY KEY NOT NULL,
	nomor_induk VARCHAR(7) NOT NULL,
	tanggal_mulai DATE NOT NULL,
	lama_cuti INT NOT NULL,
	keterangan TEXT NOT NULL
)

CREATE VIEW vewCuti_Karyawan AS SELECT * FROM cuti_karyawan

INSERT INTO cuti_karyawan(nomor_induk, tanggal_mulai, lama_cuti, keterangan)
VALUES
('IP06001', '1/2/12', 3, 'Acara Keluar'),
('IP06001', '13/2/12', 4, 'Anak Sakit'),
('IP07007', '15/2/12', 3, 'Nenek Keluar'),
('IP06003', '17/2/12', 1, 'Mendaftar Sekolah Anak'),
('IP07006', '20/2/12', 5, 'Menikah'),
('IP07004', '27/2/12', 1, 'Imunisasi Anak')

SELECT * FROM cuti_karyawan

--Soal No. 1
SELECT nama, tanggal_masuk FROM Karyawan ORDER BY tanggal_masuk ASC LIMIT 3 -- ASC Berguna untuk mengurutkan

--Soal No. 2
SELECT a.nomor_induk, a.nama, b.tanggal_mulai, b.lama_cuti, b.keterangan FROM Karyawan AS a INNER JOIN cuti_karyawan AS b 
on a.nomor_induk = b.nomor_induk

--Soal No. 3
SELECT a.nomor_induk, a.nama, SUM(b.lama_cuti) FROM Karyawan AS a RIGHT JOIN cuti_karyawan AS b
on a.nomor_induk = b.nomor_induk GROUP BY SUM a.nama, b.lama_cuti HAVING SUM(b.lama_cuti) > 1

--Soal No. 4
SELECT SUM(12-b.lama_cuti) AS sisa_cuti, a.nomor_induk, a.nama FROM Karyawan AS a LEFT JOIN cuti_karyawan AS b
on a.nomor_induk = b.nomor_induk GROUP BY a.nomor_induk, a.nama, b.lama_cuti