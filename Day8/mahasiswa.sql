CREATE TABLE mahasiswa (
	id INT PRIMARY KEY NOT NULL,
	kodemahasiswa VARCHAR(12) NOT NULL,
	namamahasiswa VARCHAR(20) NOT NULL,
	alamatmahasiswa VARCHAR(30) NOT NULL,
	kodejurusan VARCHAR(12) NOT NULL,
	kodematakuliah VARCHAR(12) NOT NULL
)

INSERT INTO mahasiswa(kodemahasiswa, namamahasiswa, alamatmahasiswa, kodejurusan, kodematakuliah)
VALUES
('MK001','Fullan','Jl. Hati besar no 63 RT.13','KA001','KPK001'),
('MK002','Fullana Binharjo','Jl. Biak kebagusan No. 34','KA002','KPK002'),
('MK003','Sardine Himura','Jl. Kebajian no 84','KA001','KPK003'),
('MK004','Isani Isabul','Jl. Merak merpati No.78','KA001','KPK001'),
('MK005','Charlie birawa','Jl. Air terjun semidi No.56','KA003','KPK002')

SELECT * FROM mahasiswa